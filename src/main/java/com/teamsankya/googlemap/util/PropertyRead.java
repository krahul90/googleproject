package com.teamsankya.googlemap.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyRead {
	String apikey = "";
	String googleurl = "";
	String clienturl = "";
	FileReader fileReader;
	String url = "";
	String queue = "";
	String mediaxml="";
	String host="";
	String portno="";
	String username="";
	String password="";

	public String getApiKey() throws IOException {

		try {
			Properties properties = new Properties();

			// for the test case use
			fileReader = new FileReader(getClass().getResource("/").getPath() + "config.properties");
			properties.load(fileReader);

			apikey = properties.getProperty("KEY");
			System.out.println(apikey);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return apikey;

	}

	public String getGoogleUrl() throws IOException {

		try {
			Properties properties = new Properties();

			// for the test case use

			fileReader = new FileReader(getClass().getResource("/").getPath() + "config.properties");
			properties.load(fileReader);

			googleurl = properties.getProperty("googleurl");
			System.out.println(googleurl);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return googleurl;
	}

	public String getClientUrl() throws IOException {

		try {
			Properties properties = new Properties();

			// for the test case use

			fileReader = new FileReader(getClass().getResource("/").getPath() + "config.properties");
			properties.load(fileReader);

			googleurl = properties.getProperty("clienturl");
			System.out.println(googleurl);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return clienturl;
	}

	public String getJmsUrl() throws IOException {

		try {
			Properties properties = new Properties();

			// for the test case use

			fileReader = new FileReader(getClass().getResource("/").getPath() + "jms.properties");
			properties.load(fileReader);

			url = properties.getProperty("URL");
			System.out.println(googleurl);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return url;
	}

	public String getqueue() throws IOException {

		try {
			Properties properties = new Properties();

			// for the test case use

			fileReader = new FileReader(getClass().getResource("/").getPath() + "jms.properties");
			properties.load(fileReader);

			queue = properties.getProperty("QUEUE");
			System.out.println(queue);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return queue;
	}
	
	public String getXml() throws IOException {

		try {
			Properties properties = new Properties();

			// for the test case use

			fileReader = new FileReader(getClass().getResource("/").getPath() + "config.properties");
			properties.load(fileReader);

			mediaxml = properties.getProperty("mediaxml");
			System.out.println(mediaxml);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return mediaxml;
	}
	public String getHost() throws IOException {

		try {
			Properties properties = new Properties();

			// for the test case use

			fileReader = new FileReader(getClass().getResource("/").getPath() + "mail.properties");
			properties.load(fileReader);

			host = properties.getProperty("host");
			System.out.println(googleurl);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return host;
	}

	public String getPortNo() throws IOException {

		try {
			Properties properties = new Properties();

			// for the test case use

			fileReader = new FileReader(getClass().getResource("/").getPath() + "mail.properties");
			properties.load(fileReader);

			portno = properties.getProperty("portno");
			System.out.println(portno);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return portno;
	}

	
	
	public String getUsername() throws IOException {

		try {
			Properties properties = new Properties();

			// for the test case use

			fileReader = new FileReader(getClass().getResource("/").getPath() + "mail.properties");
			properties.load(fileReader);

			username = properties.getProperty("username");
			System.out.println(username);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return username;
	}


	public String getPassword() throws IOException {

		try {
			Properties properties = new Properties();

			// for the test case use

			fileReader = new FileReader(getClass().getResource("/").getPath() + "mail.properties");
			properties.load(fileReader);

			password = properties.getProperty("password");
			System.out.println(password);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return password;
	}

}
