package com.teamsankya.googlemap.controller;

import java.io.IOException;
import java.util.Properties;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.teamsankya.googlemap.jaxb.GeocodeResponse;
import com.teamsankya.googlemap.util.PropertyRead;

@Controller
public class GoogleDataControl {
	// private static final String QUEUE_NAME = "Rahul";
	// private static final String URL = "tcp://localhost:61616";

	PropertyRead read = new PropertyRead();

	@RequestMapping(path = "/search", method = RequestMethod.GET)
	public String search() {
		return "search";
	}

	@RequestMapping(path = "/location", method = RequestMethod.GET)
	public String location(@RequestParam(name = "location") String location, HttpSession session)
			throws IOException, JMSException {
		Client client = ClientBuilder.newClient();
		String gurl = read.getGoogleUrl();
		System.out.println("google url" + gurl);

		WebTarget webTarget = client.target(gurl + location);
		Response response = webTarget.request().accept(read.getXml()).get();
		GeocodeResponse geo = response.readEntity(GeocodeResponse.class);
		System.out.println(geo);

		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(read.getJmsUrl());
		Connection connection = connectionFactory.createConnection();
		Session session1 = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		Destination destination = session1.createQueue(read.getqueue());
		TextMessage message = session1
	 			.createTextMessage("longname = " + geo.getResult().getAddressComponent().get(0).getLongName()
						+ " |shortname = " + geo.getResult().getAddressComponent().get(0).getShortName() + "longitude "
						+ geo.getResult().getGeometry().getLocation().getLng() + " | latitude = "
						+ geo.getResult().getGeometry().getLocation().getLat());
		MessageProducer messageProducer = session1.createProducer(destination);
		messageProducer.send(message);

	//	session1.close();
		//connection.close();
 
	session.setAttribute("location", geo);

		return "Location";
	}

	// private static final String API_KEY =
	// "eIPWXrltiPU-yYuGRN9Dj0LmpDVgZR0FsjVoi8c1vt";

	// private static final String MESSAGE = "hi and baba re baba";
	// private static final String NUMBERS = "9535228806,8116244640";

	@RequestMapping(path = "/send", method = RequestMethod.POST)
	public String send(@RequestParam(name = "longname") String longname,
			@RequestParam(name = "shortname") String shortname, @RequestParam(name = "longitude") String longitude,
			@RequestParam(name = "latitude") String latitude, @RequestParam(name = "mobile") String mobile)
			throws IOException {
		String message = "Longname--" + longname + "shortname----" + shortname + "longitude---" + longitude
				+ "latitude---" + latitude;

		Form form = new Form();

		form.param("apikey", read.getApiKey());
		form.param("message", message);
		// form.param("numbers", NUMBERS);
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target(read.getClientUrl());
		Response response = webTarget.request().accept(MediaType.APPLICATION_JSON).post(Entity.form(form));

		// System.out.println(response.readEntity(String.class));

		return "index";
	}

	//javax.mail.S0000ession session = null;

	@RequestMapping(path = "/sendmail", method = RequestMethod.POST)
	public String sendMail( @RequestParam(name = "email") String email,HttpSession session) throws IOException {
		GeocodeResponse bean = (GeocodeResponse) session.getAttribute("location");
		PropertyRead read=new PropertyRead();
		 String longname =bean.getResult().getAddressComponent().get(0).getLongName();
		 String shortname =bean.getResult().getAddressComponent().get(0).getShortName();
		 float longitude =bean.getResult().getGeometry().getLocation().getLng();
		 float latitude =bean.getResult().getGeometry().getLocation().getLat();
			
		String msg = "Longname--" + longname + "shortname----" + shortname + "longitude---" + longitude + "latitude---"
				+ latitude;

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", read.getHost());
		props.put("mail.smtp.port", read.getPortNo());

		// Get the Session object. 

		javax.mail.Session session1 = javax.mail.Session.getInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication()  {
				try {
					return new PasswordAuthentication(read.getUsername(),read.getPassword());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return null;
				}
			}
		});
		try {
			// Create a default MimeMessage object.
			Message message = new MimeMessage(session1);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(read.getUsername()));

			// Set To: header field of the header.
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));

			// Set Subject: header field
			message.setSubject("Testing Subject");

			// Now set the actual message
			message.setText(msg);

			// Send message
			Transport.send(message);
			
			System.out.println("Sent message successfully....");
		} catch (MessagingException e) {
			e.printStackTrace();

		}

		return "index";

	}    

}
