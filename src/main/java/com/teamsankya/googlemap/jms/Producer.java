package com.teamsankya.googlemap.jms;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Appender;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Layout;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;

import com.teamsankya.googlemap.util.PropertyRead;

public class Producer {
	private PropertyRead read=null;
	private static final String QUEUE_NAME = "rahul";
	private static final String URL = "tcp://localhost:61616";

	public void producer(String response ) throws JMSException{
		read= new  PropertyRead();

		Logger logger = Logger.getLogger(Producer.class);
		Layout layout=new SimpleLayout();
		Appender appender=new ConsoleAppender(layout);
		logger.addAppender(appender);

		ConnectionFactory factory = new ActiveMQConnectionFactory(URL);
		logger.info("creating connection factory object");
		Connection connection = factory.createConnection();
		logger.info("creating connectiom via factory");

		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		logger.info("creating session via connection");

		Destination destination = session.createQueue(QUEUE_NAME);
		logger.info("creating queue via session");

		
		MessageProducer producer = session.createProducer(destination);
		logger.info("creating messageProducer via session");
		producer.getClass();
		TextMessage message = session.createTextMessage(response);
		logger.info("creating TestMessage Via session");
		producer.send(message);
		
		logger.info("sending the message the activemo using producer");

	}
}
